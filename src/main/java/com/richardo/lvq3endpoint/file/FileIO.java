package com.richardo.lvq3endpoint.file;

import com.richardo.lvq3endpoint.library.LVQ3;
import com.richardo.lvq3endpoint.model.DeficiencyPattern;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

@Component
public class FileIO {
    static String excelPath = "sabrina.xlsx";

    public static void updateDataOnExcel(Object[] data, String sheetname) throws IOException,InvalidFormatException {
        FileInputStream inputStream =  new FileInputStream(new File(excelPath));
        Workbook workbook = WorkbookFactory.create(inputStream);

        Sheet sheet = workbook.getSheet(sheetname);

        int rowCount = sheet.getLastRowNum();
        Row row = sheet.createRow(++rowCount);
        int columnCount = 0;

        Cell cell  = row.createCell(columnCount);
        cell.setCellValue("K"+rowCount);

        for(Object value : data){
            cell = row.createCell(++columnCount);
            if(value instanceof Float)
                cell.setCellValue((float)value);
            if(value instanceof String)
                cell.setCellValue(String.valueOf(value));
        }

        inputStream.close();;

        FileOutputStream outputStream = new FileOutputStream(excelPath);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    public static void saveToExcel(ArrayList<ArrayList<Float>> data, String sheetName){
        try{
            FileInputStream inputStream =  new FileInputStream(new File(excelPath));
            Workbook workbook = WorkbookFactory.create(inputStream);

            Sheet sheet = workbook.getSheet(sheetName);


            if(sheet.getLastRowNum() < 2){
                int rowCount = 0;
                for(ArrayList<Float> aBook : data){
                    Row row = sheet.createRow(++rowCount);
                    int columnCount = 0;

                    Cell cell  = row.createCell(columnCount);
                    cell.setCellValue(rowCount);

                    for(float field : aBook){
                        cell = row.createCell(++columnCount);
                        cell.setCellValue(field);
                    }
                }

                inputStream.close();;

                FileOutputStream outputStream = new FileOutputStream(excelPath);
                workbook.write(outputStream);
                workbook.close();
                outputStream.close();
            }else{
                System.out.println("The codebook is exist");
            }



        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList loadDataFromExcel(String excelPath){
        ArrayList<ArrayList<Float>> dataset = new ArrayList<>();
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new File(excelPath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        int indexX = 0, indexY = 0;
        for(Row row : sheet){
            if(indexX > 0){
                String rawData = (String) dataFormatter.formatCellValue(row.getCell(107));
                String[] splitRaws = rawData.split(",");
                if(splitRaws.length == 0){
                    dataset.add(new ArrayList<Float>());
                    indexY = 0;
                    for(Cell cell: row){
                        if(indexY < 107){
                            String cellValue = dataFormatter.formatCellValue(cell);
                            dataset.get(dataset.size()-1).add(Float.valueOf((cellValue)));
                        }else{
                            dataset.get(dataset.size()-1).add((float)LVQ3.getClassNum(rawData));
                        }
                        indexY++;
                    }
                }else{
                    for(int k = 0; k < splitRaws.length; k++){
                        indexY = 0;
                        dataset.add(new ArrayList<Float>());
                        for(Cell cell: row){
                            if(indexY < 107){
                                String cellValue = dataFormatter.formatCellValue(cell);
                                dataset.get(dataset.size()-1).add(Float.valueOf((cellValue)));
                            }else{
                                dataset.get(dataset.size()-1).add((float)LVQ3.getClassNum(splitRaws[k]));
                            }
                            indexY++;
                        }
                    }

                }

            }
            indexX++;
        }
        return dataset;
    }

    public static float[][] loadCodebookFromExcel(){
        ArrayList<ArrayList<Float>> weights = new ArrayList<>();
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new File(excelPath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheet("Codebook");
        DataFormatter dataFormatter = new DataFormatter();

        int index = 0;
        for(Row row : sheet){
            index++;
            if(index > 1) {
                weights.add(new ArrayList<Float>());
                ArrayList<String> rawdata = new ArrayList<>();
                for (Cell cell : row) {
                    String cellValue = dataFormatter.formatCellValue(cell);
                    rawdata.add(String.valueOf(cellValue));
                }


                int counter = 0;
                for (String str : rawdata) {
                    if (counter > 0 && counter < 109) {
                        weights.get(weights.size()-1).add(Float.valueOf(str));
                    }
                    counter++;
                }
            }
        }
        float[][] weightsRes = new float[weights.size()][weights.get(0).size()];

        for(int i = 0; i < weights.size(); i++){
            for(int j = 0; j < weights.get(i).size(); j++){
                weightsRes[i][j] = weights.get(i).get(j);
            }
        }
        return weightsRes;
    }

    public static ArrayList loadIrisDataFromExcel(){
        //ArrayList<IrisProtoype> dataset = new ArrayList<>();
        Workbook workbook = null;
        try{
            workbook = WorkbookFactory.create(new File(excelPath));
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Sheet sheet = workbook.getSheet("Iris");
        DataFormatter dataFormatter = new DataFormatter();

        ArrayList<ArrayList<Float>> datasetArray = new ArrayList<>();

        int indexX = 0, indexY = 0;
        for(Row row : sheet){
            if(indexX > 0){
                datasetArray.add(new ArrayList<Float>());
                indexY = 0;
                for(Cell cell : row){
                    if(indexY < 4){
                        datasetArray.get(datasetArray.size()-1).add(Float.valueOf(dataFormatter.formatCellValue(cell)));
                    }
                    if (indexY == 4) {
                        if (dataFormatter.formatCellValue(cell).equals("Setosa")) {
                            datasetArray.get(datasetArray.size()-1).add(1f);
                        } else if (dataFormatter.formatCellValue(cell).equals("Versicolor")) {
                            datasetArray.get(datasetArray.size()-1).add(2f);
                        } else if (dataFormatter.formatCellValue(cell).equals("Virginica")) {
                            datasetArray.get(datasetArray.size()-1).add(3f);
                        }
                    }
                    indexY++;
                }
            }
            indexX++;
        }

        return datasetArray;
    }

    public static ArrayList kFoldLoadData(String excelPath, int num_k){
        ArrayList<ArrayList<Float>> dataset = new ArrayList<>();
        for(int i = 0 ; i < num_k; i++){

        }
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(new File(excelPath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        int indexX = 0, indexY = 0;
        for(Row row : sheet){
            if(indexX > 0){
                String rawData = (String) dataFormatter.formatCellValue(row.getCell(107));
                String[] splitRaws = rawData.split(",");
                if(splitRaws.length == 0){
                    dataset.add(new ArrayList<Float>());
                    indexY = 0;
                    for(Cell cell: row){
                        if(indexY < 107){
                            String cellValue = dataFormatter.formatCellValue(cell);
                            dataset.get(dataset.size()-1).add(Float.valueOf((cellValue)));
                        }else{
                            dataset.get(dataset.size()-1).add((float)LVQ3.getClassNum(rawData));
                        }
                        indexY++;
                    }
                }else{
                    for(int k = 0; k < splitRaws.length; k++){
                        indexY = 0;
                        dataset.add(new ArrayList<Float>());
                        for(Cell cell: row){
                            if(indexY < 107){
                                String cellValue = dataFormatter.formatCellValue(cell);
                                dataset.get(dataset.size()-1).add(Float.valueOf((cellValue)));
                            }else{
                                dataset.get(dataset.size()-1).add((float)LVQ3.getClassNum(splitRaws[k]));
                            }
                            indexY++;
                        }
                    }

                }

            }
            indexX++;
        }
        return dataset;
    }
}
