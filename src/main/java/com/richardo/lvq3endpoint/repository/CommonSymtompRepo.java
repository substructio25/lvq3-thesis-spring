package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.CommonSymtomp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Richardo Kusuma on 13/08/2019.
 */

@Repository
public interface CommonSymtompRepo extends JpaRepository<CommonSymtomp, Long> {
    
}
