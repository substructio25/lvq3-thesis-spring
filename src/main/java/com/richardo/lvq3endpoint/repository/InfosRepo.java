package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.Infos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InfosRepo extends JpaRepository<Infos, Long> {
    @Query(value = "SELECT * FROM infos WHERE micronutrient_id = :id", nativeQuery = true)
    public Infos getSingleInfoByVitId(@Param("id") Long id);


}
