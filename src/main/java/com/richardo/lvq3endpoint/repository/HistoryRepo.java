package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

@Repository
public interface HistoryRepo extends JpaRepository<History, Long> {
    @Query(value = "SELECT history.checkdate FROM deficiency.users, deficiency.history WHERE users.id = history.user_id AND users.username = :username",
    nativeQuery = true)
    public ArrayList<Date> historyDateResult(@Param("username") String username);
    @Query(value = "SELECT history.result FROM deficiency.users, deficiency.history WHERE users.id = history.user_id AND users.username = :username",
            nativeQuery = true)
    public ArrayList<String> historyResult(@Param("username") String username);
    @Query(value = "SELECT history.answers_pattern FROM deficiency.users, deficiency.history WHERE users.id = history.user_id AND users.username = :username",
            nativeQuery = true)
    public ArrayList<String> historyAnswersPattern(@Param("username") String username);
}