package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.Authorities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthorityRepo extends JpaRepository<Authorities, Long> {

}
