package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.SmartModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public interface SmartModelRepo extends JpaRepository<SmartModel, Long> {
    @Query(value = "SELECT * FROM smartmodel ORDER BY performance DESC LIMIT 1", nativeQuery = true)
    SmartModel getBestModel();

    @Query(value = "SELECT * FROM deficiency.smartmodel ORDER BY created_at DESC", nativeQuery = true)
    ArrayList<SmartModel> getEveryModel();
}
