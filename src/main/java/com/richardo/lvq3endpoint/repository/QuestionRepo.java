package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by Richardo Kusuma on 13/08/2019.
 */

@Repository
public interface QuestionRepo extends JpaRepository<Question, Long> {
//    @Query(value = "SELECT questions.question,  FROM deficiency.questions ORDER BY id ASC", nativeQuery = true)
//    ArrayList<Question> getEveryQuestions();
}
