package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.Symtomps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface SymtompRepo extends JpaRepository<Symtomps, Long> {
    @Query(value = "select * from deficiency.symtomps where symtomp like :index", nativeQuery = true)
    public ArrayList<Symtomps> findSymtopmLike(@Param("index") String index);
}
