package com.richardo.lvq3endpoint.repository;

import com.richardo.lvq3endpoint.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UserRepo extends JpaRepository<Users, Long> {
    @Query(value = "SELECT id FROM deficiency.users WHERE username = :username", nativeQuery = true)
     Long getUserId(@Param("username") String username);

    @Query(value = "SELECT * FROM deficiency.users WHERE username = :username", nativeQuery = true)
     Users getUser(@Param("username") String username);

    @Query(value = "SELECT * FROM users WHERE email = :email", nativeQuery = true)
    Users getUserByEmail(@Param("email") String email);
    
    @Query(value = "SELECT weight FROM deficiency.users WHERE username = :username", nativeQuery = true)
     int getUserWeight(@Param("username") String username);
    
    @Query(value = "SELECT height FROM deficiency.users WHERE username = :username", nativeQuery = true)
     int getUserHeight(@Param("username") String username);

}
