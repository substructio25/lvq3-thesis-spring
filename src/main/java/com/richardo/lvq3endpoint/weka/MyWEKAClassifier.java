package com.richardo.lvq3endpoint.weka;

import com.richardo.lvq3endpoint.library.LVQ3;
import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.neural.lvq.Lvq3;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.SerializationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyWEKAClassifier {
    private ArrayList<Attribute> attributes;
    private ArrayList<String> classVal;
    private Instances dataRaw;

    public MyWEKAClassifier() {
        attributes = new ArrayList<Attribute>();
        classVal = new ArrayList<String>();
        String[] classes = new String[]{"besi","flour","iodium","seng","a","b1","b2","b3","b5","b6","b7","b9","b12","c","d","e","k","fluor"};
        classVal.addAll(Arrays.asList(classes));

        for(int i = 0; i < 107; i++){
            attributes.add(new Attribute("X"+(i+1)));
        }

        attributes.add(new Attribute("class", classVal));
        dataRaw = new Instances("TestInstances", attributes, 0);
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);
    }


    public Instances createInstance(double[] newPattern) {
        dataRaw.clear();
        //double[] instanceValue1 = new double[]{petallength, petalwidth, 0};
        dataRaw.add(new DenseInstance(1.0, newPattern));
        return dataRaw;
    }


    public String classifiy(Instances insts, String path) {
        String result = "Not classified!!";
        Classifier cls = null;
        try {
            cls = (Lvq3) SerializationHelper.read(path);
            result = classVal.get((int) cls.classifyInstance(insts.firstInstance()));
        } catch (Exception ex) {
            Logger.getLogger(MyWEKAClassifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }


    public Instances getInstance() {
        return dataRaw;
    }
}
