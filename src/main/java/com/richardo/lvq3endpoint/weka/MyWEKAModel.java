package com.richardo.lvq3endpoint.weka;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.neural.common.learning.LearningKernelFactory;
import weka.classifiers.neural.lvq.Lvq3;
import weka.classifiers.neural.lvq.initialise.InitialisationFactory;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyWEKAModel {

    private double learningRate, epsilon, windowSize;

    public MyWEKAModel(double learningRate, double epsilon, double windowSize){
        this.learningRate = learningRate;
        this.epsilon = epsilon;
        this.windowSize = windowSize;
    }

    public Instances loadDataset(String path) {
        Instances dataset = null;
        try {
            dataset = ConverterUtils.DataSource.read(path);
            if (dataset.classIndex() == -1) {
                dataset.setClassIndex(dataset.numAttributes() - 1);
            }
        } catch (Exception ex) {
            Logger.getLogger(MyWEKAModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dataset;
    }

    public Classifier buildClassifier(Instances traindataset) {
        Lvq3 lvq = new Lvq3();
        lvq.setEpsilon(this.epsilon);
        lvq.setWindowSize(this.windowSize);
        lvq.setLearningRate(this.learningRate);
        lvq.setInitialisationMode(new
                SelectedTag(InitialisationFactory.INITALISE_TRAINING_EVEN,
                InitialisationFactory.TAGS_MODEL_INITALISATION)
        );
        lvq.setLearningFunction(new SelectedTag(LearningKernelFactory.LEARNING_FUNCTION_LINEAR,
                InitialisationFactory.TAGS_MODEL_INITALISATION)
        );
        lvq.setUseVoting(false);
        lvq.setSeed(1L);
        lvq.setTotalCodebookVectors(20);
        lvq.setTotalTrainingIterations(2400);

        try {
            lvq.buildClassifier(traindataset);

        } catch (Exception ex) {
            Logger.getLogger(MyWEKAModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lvq;
    }

    public String evaluateModel(Classifier model, Instances traindataset, Instances testdataset) {
        Evaluation eval = null;
        try {
            // Evaluate classifier with test dataset
            eval = new Evaluation(traindataset);
            eval.evaluateModel(model, testdataset);
        } catch (Exception ex) {
            Logger.getLogger(MyWEKAModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return String.valueOf((1-eval.errorRate()) * 100);
    }

    public void saveModel(Classifier model, String modelpath) {

        try {
            SerializationHelper.write(modelpath, model);
        } catch (Exception ex) {
            Logger.getLogger(MyWEKAModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
