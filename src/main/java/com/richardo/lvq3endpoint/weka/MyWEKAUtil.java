package com.richardo.lvq3endpoint.weka;

import weka.classifiers.neural.lvq.Lvq3;
import weka.core.Debug;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

public class MyWEKAUtil {
    public static String getPerformance(double learningRate, double epsilon, double windowSize, boolean saveModel) throws Exception {
        MyWEKAModel mg = new MyWEKAModel(learningRate, epsilon, windowSize);
        Instances dataset = mg.loadDataset("datasource/vitamin-mineral.arff");
        Filter filter = new Normalize();

        // divide dataset to train dataset 80% and test dataset 20%
        int trainSize = (int) Math.round(dataset.numInstances() * 0.8);
        int testSize = dataset.numInstances() - trainSize;

        dataset.randomize(new Debug.Random(1));// if you comment this line the accuracy of the model will be droped from 96.6% to 80%

        //Normalize dataset
        filter.setInputFormat(dataset);
        Instances datasetnor = Filter.useFilter(dataset, filter);

        Instances traindataset = new Instances(datasetnor, 0, trainSize);
        Instances testdataset = new Instances(datasetnor, trainSize, testSize);

        // build classifier with train dataset
        Lvq3 lvq = (Lvq3) mg.buildClassifier(traindataset);

        //determine to save model or not
        if(saveModel) mg.saveModel(lvq, "model.bin");

        // Evaluate classifier with test dataset
        String evalsummary = mg.evaluateModel(lvq, traindataset, testdataset);
        return evalsummary;
    }

    public static String getClassification(double[] newPattern) throws Exception {
        Filter filter = new Normalize();

        MyWEKAClassifier myWEKAClassifier = new MyWEKAClassifier();
        String classname = myWEKAClassifier.classifiy(myWEKAClassifier.createInstance(newPattern), "model.bin");
        return classname;
    }
}
