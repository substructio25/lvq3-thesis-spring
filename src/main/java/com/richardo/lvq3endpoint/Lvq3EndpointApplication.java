package com.richardo.lvq3endpoint;

import com.richardo.lvq3endpoint.api.IndexController;
import com.richardo.lvq3endpoint.api.StorageProperties;
import com.richardo.lvq3endpoint.file.FileIO;
import com.richardo.lvq3endpoint.global.WeightVectors;
import com.richardo.lvq3endpoint.library.LVQ3;
import com.richardo.lvq3endpoint.model.Infos;
import com.richardo.lvq3endpoint.repository.InfosRepo;
import com.richardo.lvq3endpoint.repository.SmartModelRepo;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

@SpringBootApplication(exclude = IntegrationAutoConfiguration.class)
public class Lvq3EndpointApplication implements CommandLineRunner {
	@Autowired
	IndexController controller;
	@Autowired
	LVQ3 lvFunc;
	@Autowired
	FileIO fileIO;
	@Autowired
	SmartModelRepo smartModelRepo;
	@Autowired
	InfosRepo infosRepo;

	public static void main(String[] args) {
		SpringApplication.run(Lvq3EndpointApplication.class, args);
	}

	@Override
	public void run(String... args) throws IOException, InvalidFormatException {
		//save the info
		//a
		Infos infosa = new Infos();
		infosa.setMicronutrient_id(5);
		infosa.setBenefits("Penting untuk penglihatan Lycopene dapat menurunkan risiko kanker prostat__" +
				"Menjaga jaringan dan kulit tetap sehat__" +
				"Berperan penting dalam pertumbuhan tulang dan sistem kekebalan tubuh__" +
				"Diet yang kaya akan karoten alfa karoten dan likopen tampaknya menurunkan risiko kanker paru-paru");
		infosa.setSources("hati sapi__telur__udang__mentega__keju cheddar");
		infosa.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b1
		Infos infosb1 = new Infos();
		infosb1.setMicronutrient_id(6);
		infosb1.setBenefits("Membantu mengubah makanan menjadi energi__" +
				"Dibutuhkan untuk kulit, rambut, otot, dan otak yang sehat__" +
				"Sangat penting untuk fungsi saraf");
		infosb1.setSources("Daging babi__beras merah__susu kedelai__semangka");
		infosb1.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b2
		Infos infosb2 = new Infos();
		infosb2.setMicronutrient_id(7);
		infosb2.setBenefits("Membantu mengubah makanan menjadi energi__" +
				"Dibutuhkan untuk kulit, rambut, otot, dan otak yang sehat");
		infosb2.setSources("Susu__telur__yogurt__keju__daging__sayuran berdaun hijau__biji-bijian utuh");
		infosb2.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b3
		Infos infosb3 = new Infos();
		infosb3.setMicronutrient_id(8);
		infosb3.setBenefits("Membantu mengubah makanan menjadi energi__" +
				"Penting untuk kulit, sel darah, otak, dan sistem saraf yang sehat");
		infosb3.setSources("Daging__unggas__ikan__biji-bijian yang diperkaya dan utuh__jamur__kentang");
		infosb3.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b5
		Infos infosb5 = new Infos();
		infosb5.setMicronutrient_id(9);
		infosb5.setBenefits("Membantu mengubah makanan menjadi energi__" +
				"Membantu membuat lipid (lemak), neurotransmiter, hormon steroid, dan hemoglobin");
		infosb5.setSources("Berbagai macam makanan bergizi, termasuk ayam__kuning telur__biji-bijian__jamur__alpukat");
		infosb5.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b6
		Infos infosb6 = new Infos();
		infosb6.setMicronutrient_id(10);
		infosb6.setBenefits("Membantu menurunkan kadar homosistein dan dapat mengurangi risiko penyakit jantung__" +
				"Membantu mengubah triptofan menjadi niasin dan serotonin, neurotransmitter yang berperan penting dalam tidur, nafsu makan, dan suasana hati__" +
				"Membantu membuat sel darah merah Mempengaruhi kemampuan kognitif dan fungsi kekebalan tubuh");
		infosb6.setSources("Daging__ikan__unggas__kacang-kacangan__tahu dan produk kedelai lainnya__kentang__buah-buahan noncitrus seperti pisang dan semangka");
		infosb6.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b7
		Infos infosb7 = new Infos();
		infosb7.setMicronutrient_id(11);
		infosb7.setBenefits("Membantu mengubah makanan menjadi energi dan mensintesis glukosa__Membantu membuat dan memecah beberapa asam lemak__Dibutuhkan untuk kesehatan tulang dan rambut");
		infosb7.setSources("Banyak makanan__termasuk biji-bijian__daging organ__kuning telur__kedelai__dan ikan");
		infosb7.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b9
		Infos infosb9 = new Infos();
		infosb9.setMicronutrient_id(12);
		infosb9.setBenefits("Penting untuk pembuatan sel baru__Membantu mencegah cacat lahir pada otak dan tulang belakang saat dikonsumsi di awal kehamilan; harus diambil secara teratur oleh semua wanita usia subur karena wanita mungkin tidak tahu mereka hamil di minggu-minggu pertama kehamilan__Dapat menurunkan kadar homocysteine \u200B\u200Bdan dapat mengurangi risiko penyakit jantung Dapat mengurangi risiko kanker usus besar__Mengimbangi risiko kanker payudara di kalangan wanita yang mengonsumsi alkohol");
		infosb9.setSources("BFortified grains and cereals__asparagus__okra__spinach__turnip greens__broccoli");
		infosb9.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//b12
		Infos infosb12 = new Infos();
		infosb12.setMicronutrient_id(13);
		infosb12.setBenefits("Penting untuk pembuatan sel baru__Membantu mencegah cacat lahir pada otak dan tulang belakang saat dikonsumsi di awal kehamilan; harus diambil secara teratur oleh semua wanita usia subur karena wanita mungkin tidak tahu mereka hamil di minggu-minggu pertama kehamilan__Dapat menurunkan kadar homocysteine \u200B\u200Bdan dapat mengurangi risiko penyakit jantung Dapat mengurangi risiko kanker usus besar__Mengimbangi risiko kanker payudara di kalangan wanita yang mengonsumsi alkohol");
		infosb12.setSources("Meat__poultry__fish__milk__cheese__eggs__fortified cereals__fortified soymilk");
		infosb12.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//c
		Infos infosc = new Infos();
		infosc.setMicronutrient_id(14);
		infosc.setBenefits("Makanan yang kaya vitamin C dapat menurunkan risiko beberapa jenis kanker, termasuk yang berasal dari mulut, kerongkongan, perut, dan payudara__Penggunaan jangka panjang dari suplemen vitamin C dapat melindungi dari katarak__Membantu membuat kolagen, jaringan ikat yang menyatukan luka dan mendukung dinding pembuluh darah__Membantu membuat neurotransmitter serotonin dan norepinefrin Bertindak sebagai antioksidan, menetralkan molekul tidak stabil yang dapat merusak sel__Mendukung sistem kekebalan tubuh");
		infosc.setSources("Fruits and fruit juices (especially citrus)__potatoes__broccoli__bell peppers__spinach__strawberries__tomatoes__Brussels sprouts");
		infosc.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//d
		Infos infosd = new Infos();
		infosd.setMicronutrient_id(15);
		infosd.setBenefits("Membantu menjaga kadar kalsium dan fosfor dalam darah, yang memperkuat tulang__Membantu membentuk gigi dan tulang__Suplemen dapat mengurangi jumlah fraktur non-tulang belakang");
		infosd.setSources("Fortified milk or margarine__fortified cereals__fatty fish");
		infosd.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//e
		Infos infose = new Infos();
		infose.setMicronutrient_id(16);
		infose.setBenefits("Bertindak sebagai antioksidan, menetralkan molekul tidak stabil yang dapat merusak sel__Melindungi vitamin A dan lipid tertentu dari kerusakan__Diet kaya vitamin E dapat membantu mencegah penyakit Alzheimer.");
		infose.setSources("Berbagai macam makanan, termasuk minyak nabati, saus salad dan margarin yang dibuat dengan minyak nabati__bibit gandum__sayuran hijau berdaun__biji-bijian utuh__kacang-kacangan");
		infose.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");
		//k
		Infos infosk = new Infos();
		infosk.setMicronutrient_id(17);
		infosk.setBenefits("Mengaktifkan protein dan kalsium yang penting untuk pembekuan darah__Dapat membantu mencegah patah tulang pinggul");
		infosk.setSources("Kubis__hati__telur__susu__bayam__brokoli__kecambah__kangkung__sawi__sayuran hijau lainnya");
		infosk.setContent_sources("https://www.health.harvard.edu/staying-healthy/listing_of_vitamins");

//		infosRepo.save(infosa);
//		infosRepo.save(infosb1);
//		infosRepo.save(infosb2);
//		infosRepo.save(infosb3);
//		infosRepo.save(infosb5);
//		infosRepo.save(infosb6);
//		infosRepo.save(infosb7);
//		infosRepo.save(infosb9);
//		infosRepo.save(infosb12);
//		infosRepo.save(infosc);
//		infosRepo.save(infosd);
//		infosRepo.save(infose);
//		infosRepo.save(infosk);


	}

	@Bean
	public LayoutDialect layoutDialect(){
		return new LayoutDialect();
	}

	//Change allowed file size

}
