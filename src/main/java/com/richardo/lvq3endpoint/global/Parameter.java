package com.richardo.lvq3endpoint.global;

public class Parameter {
    public static float ALPHA = 0.4f;
    public static float WINDOW_SIZE = 0.3f;
    public static float EPSILON = 0.2f;
    public static int NUM_OF_ITER = 3400;
    public static String EXEC_DATA_LOC = "datasource/union.xlsx";
    public static String EXEC_DATA = "datasource/training.xlsx";
    public static String TEST_DATA = "datasource/testing.xlsx";
}
