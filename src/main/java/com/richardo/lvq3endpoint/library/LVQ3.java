/*
* Make By Subtructio25
* On 23 08 2019 when fall in love with someone called Angeline Stephanie Winata*/
package com.richardo.lvq3endpoint.library;

import com.richardo.lvq3endpoint.file.FileIO;
import com.richardo.lvq3endpoint.global.Parameter;
import com.richardo.lvq3endpoint.global.WeightVectors;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Component
public class LVQ3 {
    public ArrayList startCodebook;
    public ArrayList<ArrayList<Float>> latestCodebook;

    public ArrayList<ArrayList<Float>> start(){
        //get the dataset
        ArrayList<ArrayList<Float>> fullData = FileIO.loadDataFromExcel(Parameter.EXEC_DATA_LOC);
        ArrayList<ArrayList<Float>> execData = new ArrayList<ArrayList<Float>>(fullData.subList(0, 59));

        startCodebook = lvqCodebook(execData, 5);
        latestCodebook = lvqLatestCodebook(execData, startCodebook);
        return latestCodebook;
    }

    //for init weight codebook
    public ArrayList lvqCodebook(ArrayList<ArrayList<Float>> dataset, int numOfCodebook){
        int numOfClass = getNumOfClass(dataset);
        int maxNumOfCodebook = numOfCodebook;
        int numOfCodebookVectors = numOfCodebook * numOfClass;

        ArrayList<ArrayList<Float>> codebook = new ArrayList<>();
        ArrayList<Integer> randomNumber = new ArrayList<>();
        Random random = new Random();
        int rand = 0;
        while(true){
            rand = random.nextInt(dataset.size());
            //set the exist truth
            boolean isExist = false;
            //the codebook still empty add it first
            if(codebook.size() == 0) codebook.add(dataset.get(rand));
            else{
                //save the chosen instances first
                ArrayList<Float> chosenInstances = dataset.get(rand);
                //check the num of current data class instances
                int classNum = numOfClass(dataset, chosenInstances.get(chosenInstances.size()-1));
                //check the existence of current instances in codebook array
                for(ArrayList<Float> singleCodebook : codebook){
                    if(chosenInstances.toString().equals(singleCodebook.toString())){
                        //is exist
                        isExist = true;
                    }
                }

                int maxNumofCurrentCodebook = maxNumOfCodebook;

                if(classNum < maxNumOfCodebook){
                    //change the maximum allowed of current class if it exits below the max allowance
                    maxNumofCurrentCodebook =  classNum;
                    //change the max num of codebook each class
                    numOfCodebookVectors -= (maxNumOfCodebook - classNum);
                }

                //check that the codebook still can represent
                if(!isExist)
                    //check that maximum num of represents still allowed
                    if(numOfClass(codebook, chosenInstances.get(chosenInstances.size()-1)) <= maxNumofCurrentCodebook)
                        codebook.add(dataset.get(rand));
                if(codebook.size() >= numOfCodebookVectors) break;

            }
        }

        return codebook;
    }

    //get final weight codebook
    public ArrayList lvqLatestCodebook(ArrayList<ArrayList<Float>> dataset, ArrayList<ArrayList<Float>> codebook){
        ArrayList<ArrayList<Float>> tempCodebook = copyArray(codebook);
        ArrayList<Float> inputVector = null;
        float alpha = Parameter.ALPHA;
        for(int start = 0; start < Parameter.NUM_OF_ITER; start++){
            //while(alpha > Parameter.MIN_ALPHA){
            for(int i = 0; i < dataset.size(); i++){
                inputVector = dataset.get(i);
                ArrayList<Float> eu_dist = new ArrayList<>();
                for(int j = 0; j < tempCodebook.size(); j++)
                    eu_dist.add(euclidian(inputVector, tempCodebook.get(j)));

                float smallest = 10000;
                int ci = 0;
                for(int j = 0 ; j < eu_dist.size(); j++){
                    if(eu_dist.get(j) < smallest){
                        ci = j;
                        smallest = eu_dist.get(j);
                    }
                }

                smallest = 10000;
                int cj = 0;
                for(int j = 0; j < eu_dist.size(); j++){
                    if(j != ci){
                        if(eu_dist.get(j) < smallest){
                            cj = j;
                            smallest = eu_dist.get(j);
                        }
                    }
                }

                int T = (int)(float)dataset.get(i).get(dataset.get(i).size()-1);
                float di = eu_dist.get(ci);
                float dj = eu_dist.get(cj);

                if(ci == cj && (float)cj == T){
                    for(int j = 0; j < tempCodebook.get(ci).size()-1; j++)
                        codebook.get(ci).set(j, tempCodebook.get(ci).get(j) + Parameter.EPSILON * alpha * (inputVector.get(j)-codebook.get(ci).get(j)));
                    for(int j = 0; j < tempCodebook.get(cj).size()-1; j++)
                        codebook.get(cj).set(j, tempCodebook.get(cj).get(j) + Parameter.EPSILON * alpha * (inputVector.get(j)-codebook.get(cj).get(j)));
                }else if(ci != cj){
                    if(ci == T || cj == T){
                        if(minimumValue(di/dj, dj/di) > (1-Parameter.WINDOW_SIZE)/(1+Parameter.WINDOW_SIZE)){
                            if(ci == T){
                                for(int j = 0; j < tempCodebook.get(ci).size()-1; j++)
                                    codebook.get(ci).set(j, tempCodebook.get(ci).get(j) + alpha * (inputVector.get(j) - codebook.get(ci).get(j)));
                                for(int j = 0; j < tempCodebook.get(cj).size()-1; j++)
                                    codebook.get(cj).set(j, tempCodebook.get(cj).get(j) - alpha * (inputVector.get(j)-codebook.get(cj).get(j)));
                            }else if(cj == T){
                                for(int j = 0; j < tempCodebook.get(ci).size()-1; j++)
                                    codebook.get(ci).set(j, tempCodebook.get(ci).get(j) - alpha * (inputVector.get(j)-codebook.get(ci).get(j)));
                                for(int j = 0; j < tempCodebook.get(cj).size()-1; j++)
                                    codebook.get(cj).set(j, tempCodebook.get(cj).get(j) + alpha * (inputVector.get(j)-codebook.get(cj).get(j)));
                            }
                        }
                    }
                }
            }
            alpha -= alpha * 0.01f;
        }
        return codebook;
    }

    public int numOfClass(ArrayList<ArrayList<Float>> data, float classA){
        int num = 0;
        for(ArrayList<Float> singleData : data){
            if(singleData.get(singleData.size()-1) == classA)
                num++;
        }
        return num;
    }

    public String getSinglePredResult(ArrayList<Float> inputVector){
        ArrayList<Float> eu_dist = new ArrayList<>();
        for(int j = 0; j < WeightVectors.weights.size(); j++)
            eu_dist.add(euclidian(inputVector, WeightVectors.weights.get(j)));

        float smallest = 10000;
        int ci = 0;
        for(int j = 0 ; j < eu_dist.size(); j++){
            if(eu_dist.get(j) < smallest){
                ci = j;
                smallest = eu_dist.get(j);
            }
        }
        float classResult = WeightVectors.weights.get(ci).get(WeightVectors.weights.get(ci).size()-1);
        return getClassString((int)classResult);
    }

    public int getNumOfClass(ArrayList<ArrayList<Float>> dataset){
        HashSet<Float> classSet = new HashSet<>();
        for(ArrayList<Float> list : dataset){
            classSet.add(list.get(list.size()-1));
        }
        return classSet.size();
    }

    public float euclidian(ArrayList<Float> X, ArrayList<Float> W){
        float sum = 0;
        if(X.size() == W.size()){
            for(int i = 0; i < X.size(); i++){
                sum += Math.pow((W.get(i)- X.get(i)), 2);
            }
        }
        sum = (float)Math.sqrt(sum);
        return sum;
    }

    public float minimumValue(float a1, float a2){
        if(a1 < a2) return a1;
        return a2;
    }

    public float nearestClass(ArrayList<ArrayList<Float>> codebook, ArrayList<Float> inputVector){
        float smallest = 10000;
        int ci = 0;
        int index = 0;

        for(ArrayList<Float> cbook : codebook){
            float euclidianResult = euclidian(inputVector, cbook);
            if(euclidianResult < smallest){
                smallest = euclidianResult;
                ci = index;
            }
            index++;
        }
        return codebook.get(ci).get(codebook.get(ci).size()-1);
    }

    public static class LVQResult{
        public Float target;
        public Float result;
        public LVQResult(Float target, Float result){
            this.target = target;
            this.result = result;
        }

        public LVQResult() {
        }

        public Float getTarget() {
            return target;
        }

        public void setTarget(Float target) {
            this.target = target;
        }

        public Float getResult() {
            return result;
        }

        public void setResult(Float result) {
            this.result = result;
        }
    }

    public ArrayList lvqTestingResult(ArrayList<ArrayList<Float>> dataset, ArrayList<ArrayList<Float>> codebook){
        ArrayList<LVQResult> lvqResultArray = new ArrayList();
        float num = 0;
        for(int i = 0; i < dataset.size(); i++){
            lvqResultArray.add(new LVQResult(dataset.get(i).get(dataset.get(i).size()-1),
                    nearestClass(codebook, dataset.get(i))));
            if(dataset.get(i).get(dataset.get(i).size()-1) ==  nearestClass(codebook, dataset.get(i)))
                num++;
        }

        lvqResultArray.add(new LVQResult(10000f, (num/(float)dataset.size())));
        return lvqResultArray;
    }

    public float percentageOfTruth(ArrayList<LVQResult> test){
        float num = 0;
        for(LVQResult res : test){
            if(res.result == res.target){
                System.out.println("Class are same");
                num++;
            }
        }
        return num;
    }

    public String getClassString(int i){
        switch(i){
            case 1: return "besi";
            case 2: return "fluor";
            case 3: return "iodium";
            case 4: return "seng";
            case 5: return "a";
            case 6: return "b1";
            case 7: return "b2";
            case 8: return "b3";
            case 9: return "b5";
            case 10: return "b6";
            case 11: return "b7";
            case 12: return "b9";
            case 13: return "b12";
            case 14: return "c";
            case 15: return "d";
            case 16: return "e";
            case 17: return "k";
            default: return "none";
        }
    }

    public static int getClassNum(String str){
        if(str.equals("besi")) return 1;
        else if(str.equals("fluor")) return 2;
        else if(str.equals("iodium")) return 3;
        else if(str.equals("seng")) return 4;
        else if(str.equals("a")) return 5;
        else if(str.equals("b1")) return 6;
        else if(str.equals("b2")) return 7;
        else if(str.equals("b3")) return 8;
        else if(str.equals("b5")) return 9;
        else if(str.equals("b6")) return 10;
        else if(str.equals("b7")) return 11;
        else if(str.equals("b9")) return 12;
        else if(str.equals("b12")) return 13;
        else if(str.equals("c")) return 14;
        else if(str.equals("d")) return 15;
        else if(str.equals("e")) return 16;
        else if(str.equals("k")) return 17;
        else return 0;
    }

    public static ArrayList<ArrayList<Float>> copyArray( ArrayList<ArrayList<Float>> array){
        ArrayList<ArrayList<Float>> arrayToReturn = new ArrayList<>();
        for(int i = 0; i < array.size(); i++){
            arrayToReturn.add(new ArrayList<Float>());
            ArrayList<Float> currentArray = arrayToReturn.get(i);
            for(int j = 0; j < array.get(i).size(); j++){
                currentArray.add(array.get(i).get(j));
            }
        }
        return arrayToReturn;
    }
}
