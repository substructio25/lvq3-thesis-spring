package com.richardo.lvq3endpoint.controller;

import com.richardo.lvq3endpoint.file.FileIO;
import com.richardo.lvq3endpoint.library.LVQ3;
import com.richardo.lvq3endpoint.model.*;
import com.richardo.lvq3endpoint.repository.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.*;

@Controller
public class IndexWebController {
    @Autowired
    IndexWebController controller;
    @Autowired
    InfosRepo infosRepo;
    @Autowired
    HistoryRepo historyRepo;
    @Autowired
    SymtompRepo symtompRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    AuthorityRepo authRepo;
    @Autowired
    public LVQ3 lvFunc;
    @Autowired
    public BCryptPasswordEncoder passwordEncoder;
    @Autowired
    public SmartModelRepo smartModelRepo;

    @GetMapping("/")
    public String getHome(){
        return "redirect:/infos";
    }

    @GetMapping("/infos")
    public String getInfos(Model attribute){
        return "infos";
    }

    @GetMapping("/infos/{vitid}")
    public String getMoreInfos(Model attribute, @PathVariable("vitid") Long vitid){
        Infos info = infosRepo.getSingleInfoByVitId(vitid);
        System.out.println(info.getBenefits());
        String[] benefits = info.getBenefits().split("__");
        String[] sources= info.getSources().split("__");
        String infoVit = lvFunc.getClassString(Math.toIntExact(vitid));
        attribute.addAttribute("benefits", benefits);
        attribute.addAttribute("micronutrient", infoVit);
        attribute.addAttribute("sources", sources);
        attribute.addAttribute("imgSrc", info.getImg_source());
        return "moreinfos";
    }

    @GetMapping("/infos/history")
    public String getHistory(Model attribute){
        ArrayList<Date> dates;
        ArrayList<String> results;
        ArrayList<ArrayList<String>> readablePattern = new ArrayList<>();

        //get the authenticated user
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "kusuma";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        results = historyRepo.historyResult(username);
        dates = historyRepo.historyDateResult(username);
        for(String str : historyRepo.historyAnswersPattern(username)){
            String[] strSplit = str.split(",");
            readablePattern.add(new ArrayList<String>());
            ArrayList currentArray = readablePattern.get(readablePattern.size()-1);
            for(int i = 0; i < strSplit.length; i++){
                if(strSplit[i].equals("1")){
                    currentArray.add(symtompsString(Integer.valueOf(i)+1));
                }
            }
            if(currentArray.size() < 1){
                currentArray.add("You are healthy");
            }
        }

        attribute.addAttribute("historyDates", dates);
        attribute.addAttribute("historyResults", results);
        attribute.addAttribute("historySymptomps", readablePattern);
        return "history";
    }

    @GetMapping("/about")
    public String getAbout(){
        return "aboutinfo";
    }

    @GetMapping("/about/testing")
    public String getTestingAbout(){
        return "abouts";
    }

    @GetMapping("/admin/performance")
    public String getModelPerformance(Model attribute){
        //attribute.addAttribute("")
        attribute.addAttribute("performances", smartModelRepo.getEveryModel());
        attribute.addAttribute("bestPerf", smartModelRepo.getBestModel());
        return "performance";
    }

    //class for upload form
    @Getter @Setter
    class UploadForm{ private MultipartFile[] files; private String type;}
    //end class

    @GetMapping("/questions")
    public String getQuestion(){
        return "questions";
    }

    @GetMapping("/quick/result/{vitamin}")
    public String getResult(Model attribute, @PathVariable("vitamin") int vitamin){
        String resVit = lvFunc.getClassString(vitamin);
        attribute.addAttribute("result", resVit);
        attribute.addAttribute("vitId", vitamin);
        return "checkresult";
    }

    @GetMapping("/forgot")
    public String getForgotPassword(){
        return "forgot";
    }

    @PostMapping("/forgot-password")
    public String postForgotPassword(@RequestParam("email") String email,
                                     @RequestParam("password") String password,
        @RequestParam("confirm-password") String confirmPassword, Model attribute){
        Users userFetched = userRepo.getUserByEmail(email);

            if(password.equals(confirmPassword)){
                Users user = userFetched;
                user.setPassword(passwordEncoder.encode(password));
                userRepo.save(user);
                return "redirect:/login";
            }else {
                attribute.addAttribute("error",  true);
                return "forgot";
            }
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String getProfile(Users user, Model attribute){
        //get the authenticated user
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "kusuma";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        user.setUsername(username);
        user.setHeight(userRepo.getUserHeight(username));
        user.setWeight(userRepo.getUserWeight(username));
        attribute.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping(value = "/profile/update", method = RequestMethod.GET)
    public String postProfile(Users user){
        Users userToSave = userRepo.getUser(user.username);
        userToSave.setUsername(user.getUsername());
        userToSave.setWeight(user.getWeight());
        userToSave.setHeight(user.getHeight());
        userRepo.save(userToSave);
        return "redirect:/profile";
    }

    @GetMapping("/signup")
    public String gotSignUp(Model attribute){
        attribute.addAttribute("user", new Users());
        return "signup";
    }

    @PostMapping("/signup")
    public String giveSignUp(Model attribute, Users user) {
        //user.setPassword(passwordEncoder.encode(user.password));
        user.setEnabled(true);
        Authorities auts = new Authorities();
        auts.username = user.username;
        auts.authority = "ROLE_USER";
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        System.out.println(user.getPassword());
        userRepo.save(user);
        authRepo.save(auts);
        return "redirect:/signin";
    }

    public String symtompsString(int symtompsNumber){
        switch(symtompsNumber){
            case 1: return "Mata (penglihatan) berkunang-kunang";
            case 2: return "Bagian dalam kelopak mata tampak pucat";
            case 3: return "Nafas Terengah-engah";
            case 4: return "5L (lemah, letih, lesu, lelah, lunglai)";
            case 5: return "Kulit telapak tangan tampak pucat";
            case 6: return "Kurang konsentrasi dalam bekerja atau belajar";
            case 7: return "Rasa gelisah dan perubahan suasana hati";
            case 8: return "Kuku rapuh";
            case 9: return "Kerontokan rambut";
            case 10: return "Daya tahan tubuh berkurang atau menurun";
            case 11: return "Gangguan penyembuhan luka";
            case 12: return "Pertumbuhan terhambat";
            case 13: return "Sulit melihat dalam keadaan gelap";
            case 14: return "Kurang nafsu makan";
            case 15: return "Ketajaman indera pengecap berkurang ";
            case 16: return "Nyeri saat menelan makanan ";
            case 17: return "Sering merasa mual dan muntah";
            case 18: return "Timbul tonjolan kecil pada kelenjar tiroid dan membengkak atau membesar";
            case 19: return "Kulit terasa kering dan kasar";
            case 20: return "Nyeri pada sendi dan otot ";
            case 21: return "Tinggal di daerah atau kawasan pegunungan";
            case 22: return "Karies pada gigi";
            case 23: return "Tinggal di daerah sulit air bersih ";
            case 24: return "Bagian putih bola mata tampak kering";
            case 25: return "Bagian putih bola mata tampak keriput ";
            case 26: return "Bagian putih bola mata terlihat kasar dan kusam";
            case 27: return "Bagian putih bola mata memilik bercak seperti busa sabun atau keju";
            case 28: return "Permukaan kornea terlihat kering dan keruh ";
            case 29: return "Kornea terlihat lembek dan menonjol";
            case 30: return "Penglihatan menurun saat senja tiba atau tidak dapat melihat di lingkungan yang kurang cahaya";
            case 31: return "Bercak-bercak putih pada permukaan dalam mata";
            case 32: return "Kornea mata menjadi putih ";
            case 33: return "Kulit tampak kering dan kasar";
            case 34: return "Mudah terkena infeksi pada saluran pernafasan";
            case 35: return "Mudah terkena infeksi pada permukaan dinding usus yang menyebabkan diare";
            case 36: return "Rambut kering";
            case 37: return "Pembengkakan yang mengandung nanah di daerah telinga";
            case 38: return "Sulit untuk tidur";
            case 39: return "Mudah kelelahan";
            case 40: return "Bentuk gigi tidak teratur dan mudah rusak";
            case 41: return "Gerakan mata yang tidak terkendali";
            case 42: return "Mudah bertengkar";
            case 43: return "Selalu merasa khawatir";
            case 44: return "Penglihatan kabur karena katarak";
            case 45: return "Bagian bibir kelopak mata terasa panas dan gatal";
            case 46: return "Mata sensitif terhadap cahaya";
            case 47: return "Sudut mulut pecah-pecah";
            case 48: return "Kurangnya ketajaman bibir dan mulut (perasa)";
            case 49: return "Lidah sakit dan panas";
            case 50: return "Penebalan kulit (kapalan)";
            case 51: return "Penggelapan warna kulit abnormal pasca peradangan kulit";
            case 52: return "Hiperaktivitas sampai maniak";
            case 53: return "Perasaan seperti terbakar pada dada";
            case 54: return "Sering lupa akan sesuatu (pikun)";
            case 55: return "Keliru";
            case 56: return "Perubahan emosi dan kepribadian";
            case 57: return "Emosi yang labil";
            case 58: return "Berat badan menurun";
            case 59: return "Suka mengigau";
            case 60: return "Kehilangan konsentrasi";
            case 61: return "Kejang-kejang";
            case 62: return "Kebotakan setempat";
            case 63: return "Rasa lemah dan panas pada kaki";
            case 64: return "Apatis";
            case 65: return "Merasa tidak nyaman dalam bergerak (kelemahan otot)";
            case 66: return "Nafsu makan berkurang";
            case 67: return "Ketakutan";
            case 68: return "Cepat lelah";
            case 69: return "Rasa kesemutan pada kaki atau tangan";
            case 70: return "Mudah tersinggung";
            case 71: return "Mudah marah";
            case 72: return "Nafas pendek";
            case 73: return "Jantung berdebar lebih kuat dan tidak teratur";
            case 74: return "Refleks berkurang";
            case 75: return "Kulit kering, berwarna merah, bersisik dan bisa mengelupas";
            case 76: return "Lidah licin, membengkak dan berwarna gelap";
            case 77: return "Perasaan tidak nyaman pada perut";
            case 78: return "Mual dan rasa ingin muntah";
            case 79: return "Gelisah sampai depresi";
            case 80: return "Kebingungan sampai haluinasi";
            case 81: return "Diare";
            case 82: return "Susah tidur";
            case 83: return "Rambut beruban dan rontok";
            case 84: return "Kepala pusing";
            case 85: return "Kejang otot";
            case 86: return "Otot atau persendian sakit";
            case 87: return "Kulit kering, kasar, dan gatal";
            case 88: return "Warna merah kebiruan pada kulit";
            case 89: return "Pendarahan pada gusi";
            case 90: return "Dudukan gigi menjadi longgar";
            case 91: return "Mulut kering";
            case 92: return "Mata kering";
            case 93: return "Sering merasa gugup";
            case 94: return "Sering mengalami kram pada kaki";
            case 95: return "Mengalami mati rasa pada kaki dan tangan";
            case 96: return "Tulang punggung bungkuk";
            case 97: return "Tulang kaki membengkok dengan bentuk O atau X ";
            case 98: return "Ujung-ujung tulang panjang membesar (lutut dan pergelangan) ";
            case 99: return "Tulang dada tampak menonjol";
            case 100: return "Pembusukan pada gigi";
            case 101: return "Banyak mengeluarkan keringat";
            case 102: return "Merasakan sakit seperti rematik";
            case 103: return "Sering mengalami patah tulang";
            case 104: return "Merasa sakit ketika bergerak (lemah otot";
            case 105: return "Sulit berjalan/kehilangan kordinasi";
            case 106: return "Penglihatan ganda";
            case 107: return "Darah sukar membeku";
            default: return "none";
        }
    }

    public int getIndexClass(String species){
        switch(species){
            case "besi": return 0;
            case "fluor": return 1;
            case "iodium": return 2;
            case "seng": return 3;
            case "a": return 4;
            case "b1": return 5;
            case "b2": return 6;
            case "b3": return 7;
            case "b5": return 8;
            case "b6": return 9;
            case "b7": return 10;
            case "b9": return 11;
            case "b12": return 12;
            case "c": return 13;
            case "d": return 14;
            case "e": return 15;
            case "k": return 16;
            default: return -1;
        }
    }
}
