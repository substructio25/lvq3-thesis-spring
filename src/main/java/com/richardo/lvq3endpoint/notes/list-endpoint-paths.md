## List of Paths
* /quick/search/symtomp -> mencari gejala untuk fitur quick prediction
* /quick/check -> menampilkan hasil prediksi untuk fitur
* /list/symtomp/all -> menampilkan setiap gejala dari database
* /list/questions -> menampilkan setiap pertanyaan berdasarkan gejala-gejala
* /quick/result -> menampilkan hasil dari quick prediction
* /infos -> menampilkan pengethuan dasar mengenai vitamin dan mineral
* /infos/{vitid} -> menampilkan info mengenai vitamin yang lebih spesifik
* "/infos/history -> menampilkan riwayat pemeriksaan