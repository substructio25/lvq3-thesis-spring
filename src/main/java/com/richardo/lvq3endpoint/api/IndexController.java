/*
* This Program is Written
* By Richardo Kusuma
* When falling in love with Ruth Stephanie Winata
* */
package com.richardo.lvq3endpoint.api;

import com.richardo.lvq3endpoint.file.FileIO;
import com.richardo.lvq3endpoint.global.Parameter;
import com.richardo.lvq3endpoint.global.WeightVectors;
import com.richardo.lvq3endpoint.library.LVQ3;
import com.richardo.lvq3endpoint.library.StrCprs;
import com.richardo.lvq3endpoint.model.*;
import com.richardo.lvq3endpoint.repository.*;
import com.richardo.lvq3endpoint.weka.MyWEKAModel;
import com.richardo.lvq3endpoint.weka.MyWEKAUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
public class IndexController {
    public static final String SAMPLE_EXCEL_PATH="sabrina.xlsx";
    public ArrayList<DeficiencyPattern> data_set = new ArrayList<>();
    public Random random = new Random();
    @Autowired public IndexController controller;
    @Autowired public SymtompRepo symtompRepo;
    @Autowired public HistoryRepo historyRepo;
    @Autowired public UserRepo userRepo;
    @Autowired public LVQ3 lvFunc;
    @Autowired public QuestionRepo questionRepo;
    @Autowired public SmartModelRepo smartModelRepo;
    @Autowired public StrCprs strCprs;

    @RequestMapping(value = "/history/save", method = RequestMethod.GET)
    public String saveHistory(@RequestParam("answers") String answers, @RequestParam("result") String result){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = null;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        //save the history to database
        History history = new History();
        history.setUser_id(Math.toIntExact(userRepo.getUserId(username)));
        history.setResult(result);
        history.setAnswers_pattern(answers);
        historyRepo.save(history);

        return "Success Save History";
    }

    @RequestMapping("/get/questions")
    public List getSymtompsQuestions(){
        return questionRepo.findAll();
    }

    //For WEKA generating model
    @RequestMapping("/generate/model")
    public String generateModelForWEKA(@RequestParam("learning_rate") float learningRate,
                                @RequestParam("window_size") float winSize,
                                @RequestParam("epsilon") float epsilon) throws Exception {
        //set the parameter for training
        if(learningRate != 0) Parameter.ALPHA = learningRate;
        if(winSize != 0) Parameter.WINDOW_SIZE = winSize;
        if(epsilon != 0) Parameter.EPSILON = epsilon;

        String performance = MyWEKAUtil.getPerformance(learningRate, epsilon,winSize, true);

        //get the authenticated user
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "kusuma";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        //save the model to database
        SmartModel smartModel = new SmartModel();
        smartModel.setUsers(userRepo.getUser(username));
        smartModel.setPerformance(Float.parseFloat(performance));
        smartModel.setCodebook(null);
        smartModel.setLearning_rate(Parameter.ALPHA);
        smartModel.setEpsilon(Parameter.EPSILON);
        smartModel.setWin_size(Parameter.WINDOW_SIZE);
        smartModelRepo.save(smartModel);

        return "Generation Successful";
    }
    //end for


    public String generateModel(@RequestParam("learning_rate") float learningRate,
                                @RequestParam("window_size") float winSize,
                                @RequestParam("epsilon") float epsilon) throws IOException {
        //set the parameter for training
        if(learningRate != 0) Parameter.ALPHA = learningRate;
        if(winSize != 0) Parameter.WINDOW_SIZE = winSize;
        if(epsilon != 0) Parameter.EPSILON = epsilon;

        //get the latest codebook
        ArrayList<ArrayList<Float>> latestCodebook = lvFunc.start();
        System.out.println("The codebook generation");
        System.out.println(latestCodebook.size());
        System.out.println(latestCodebook.get(0).size());

        //the test data
        ArrayList<ArrayList<Float>> fullData = FileIO.loadDataFromExcel(Parameter.EXEC_DATA_LOC);
        ArrayList<ArrayList<Float>> testData = new ArrayList<ArrayList<Float>>(fullData.subList(60, 86));

        ArrayList<LVQ3.LVQResult> trainResult = lvFunc.lvqTestingResult(testData, latestCodebook);
        System.out.println("The train result");
        for(int i=0; i<trainResult.size(); i++){
            System.out.println("Target "+trainResult.get(i).getTarget()+" Result "+trainResult.get(i).getResult());
        }

        //ArrayList testTrainToTrain = lvFunc.lvqTestingResult(FileIO.loadDataFromExcel(Parameter.EXEC_DATA_LOC), latestCodebook);
        //set the global weight vectors
        WeightVectors.weights = latestCodebook;

        //get the performance results
        LVQ3.LVQResult trainToTest = ( LVQ3.LVQResult)trainResult.get(trainResult.size()-1);
        System.out.println("Latest result " + trainToTest.getResult());
        //get the authenticated user
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "kusuma";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        //save the model to database
        SmartModel smartModel = new SmartModel();
        smartModel.setUsers(userRepo.getUser(username));
        smartModel.setPerformance((float)trainToTest.getResult());
        smartModel.setCodebook(convertWeightToString(latestCodebook));
        smartModel.setLearning_rate(Parameter.ALPHA);
        smartModel.setEpsilon(Parameter.EPSILON);
        smartModel.setWin_size(Parameter.WINDOW_SIZE);
        smartModelRepo.save(smartModel);

        return "Generation Successful";
    }

    @RequestMapping("/get/model_best")
    public SmartModel getBestSmartModel(){
        SmartModel bestModel = smartModelRepo.getBestModel();
        Parameter.ALPHA = bestModel.getLearning_rate();
        Parameter.EPSILON = bestModel.getEpsilon();
        Parameter.WINDOW_SIZE = bestModel.getWin_size();
        return smartModelRepo.getBestModel();
    }

    @RequestMapping("/get/performances")
    public ArrayList getPerformances(){
        return smartModelRepo.getEveryModel();
    }

    @Getter @Setter
    class UploadForm{
        private MultipartFile files;
        private String type;
    }

    @RequestMapping(value = "/upload/data")
    public String getExecTestData(UploadForm uploadForm){
        System.out.println(uploadForm.type);
        return "Data Imported";
    }

    @RequestMapping("/data/exec")
    public ArrayList dataExercise(){
        return FileIO.loadDataFromExcel(Parameter.EXEC_DATA_LOC);
    }

    //will implement weka
    @RequestMapping("/results/check")
    public String classifyNewData(@RequestParam("answers[]") String[] answers) throws Exception {
        double[] newPattern = new double[108];
        for(int i = 0; i < answers.length; i++)
            newPattern[i] = Double.parseDouble(answers[i]);
        newPattern[newPattern.length-1] = 0;
        return MyWEKAUtil.getClassification(newPattern);
    }
//    public String checkTheCurrentAnwerPattern(@RequestParam("answers[]") String[] answers){
//        System.out.println(answers.toString());
//        ArrayList<Float> answersPattern = new ArrayList<>();
//        for(int i = 0; i < answers.length; i++)
//            answersPattern.add(Float.valueOf(answers[i]));
//
//        return lvFunc.getSinglePredResult(answersPattern);
//    }

    public String convertWeightToString(ArrayList<ArrayList<Float>> array){
        String weightString = "";
        for(int i = 0; i < array.size(); i++){
            for(int j = 0; j < array.get(i).size(); j++){
                if(j == array.get(i).size()-1)
                    weightString += array.get(i).get(j);
                else weightString += array.get(i).get(j)+" ";
            }
            weightString += ",";
        }
        return weightString;
    }
}