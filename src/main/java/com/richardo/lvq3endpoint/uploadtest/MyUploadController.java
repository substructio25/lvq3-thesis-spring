package com.richardo.lvq3endpoint.uploadtest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MyUploadController {
    @RequestMapping(value = "/uploadFile", method = RequestMethod.GET)
    public String uploadFile(){
        //MyUploadForm myUploadForm = new MyUploadForm();
        //model.addAttribute("myUploadForm", myUploadForm);
        return "test/upload";
    }

    @RequestMapping(value="/uploadFile", method = RequestMethod.POST)
    public String postFile(HttpServletRequest request, Model model, @RequestParam("fileDatas[]") MultipartFile[] file,
       @RequestParam("description") String description){

        String uploadRootPath = request.getServletContext().getRealPath("upload");
        System.out.println("uploadRootPath="+uploadRootPath);
        File uploadRootDir = new File(uploadRootPath);

        if(!uploadRootDir.exists()){
            uploadRootDir.mkdirs();
        }

        MultipartFile[] fileDatas = file;

        List<File> uploadedFiles = new ArrayList<File>();

        for(MultipartFile fileData : fileDatas){
            String name = fileData.getOriginalFilename();

            if(name != null && name.length() > 0){
                try{
                    File serverFile = new File(uploadRootDir.getAbsoluteFile()+File.separator+name);

                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();

                    uploadedFiles.add(serverFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        model.addAttribute("description", description);
        model.addAttribute("uploadedFiles", uploadedFiles);
        return "test/result";
    }

}
