package com.richardo.lvq3endpoint.test;

import com.richardo.lvq3endpoint.file.FileIO;
import com.richardo.lvq3endpoint.global.Parameter;
import com.richardo.lvq3endpoint.library.LVQ3;

import java.lang.reflect.Array;
import java.util.*;

public class Lvq3TestMain {
    public static void main(String... args){
        //read dataset
        ArrayList<ArrayList<Float>> fullData = FileIO.loadDataFromExcel(Parameter.EXEC_DATA_LOC);
        ArrayList<ArrayList<Float>> execData =  FileIO.loadDataFromExcel(Parameter.EXEC_DATA);
        ArrayList<ArrayList<Float>> testData =  FileIO.loadDataFromExcel(Parameter.TEST_DATA);
        //ArrayList<ArrayList<Float>> testData = FileIO.loadDataFromExcel(Parameter.TEST_DATA_LOC);
        //normalize dataset
        //randomize dataset

        //init codebook

        System.out.println("Number of Codebook ");
        System.out.println(getNumOfClass(execData));


        int numOfClass = getNumOfClass(execData);
        int maxNumOfCodebook = 2;
        int numOfCodebookVectors = maxNumOfCodebook * numOfClass;
        //System.out.println("Seed" +l);
        ArrayList<ArrayList<Float>> codebook = new ArrayList<>();
        ArrayList<Integer> indexAdded = new ArrayList<>();

        int numOfCodebook = 20;

        float num = 0;
        Random random = new Random();
        random.setSeed(20);

        for (int i = 0; i < numOfCodebook; i++) {
            if (num >= getNumOfClass(execData)) num -= getNumOfClass(execData);
            num++;

            //adding the codebook based on the class

            //random.setSeed(1);

            while (true) {
                int rnd = random.nextInt(execData.size());
                if (codebook.size() == 0) {
                    if (execData.get(rnd).get(execData.get(rnd).size() - 1) == num) {
                        codebook.add(execData.get(rnd));
                        break;
                    }
                } else {
                    boolean toStop = false;
                    for (ArrayList<Float> singleCB : codebook) {
                        if (!singleCB.toString().equals(execData.get(rnd).toString())) {
                            if (execData.get(rnd).get(execData.get(rnd).size() - 1) == num) {
                                codebook.add(execData.get(rnd));
                                toStop = true;
                                break;
                            }
                        }
                    }
                    if (toStop) break;
                }
            }
        }

        System.out.println("The Codebook");
        System.out.println(codebook.size());

        //exec model to get final codebook
        ArrayList<ArrayList<Float>> tempCodebook = copyArray(codebook);
        ArrayList<Float> inputVector = null;
        float alpha = Parameter.ALPHA;
        for (int start = 0; start < Parameter.NUM_OF_ITER; start++) {
            //while(alpha > Parameter.MIN_ALPHA){
            for (int i = 0; i < execData.size(); i++) {
                inputVector = execData.get(i);
                ArrayList<Float> eu_dist = new ArrayList<>();
                for (int j = 0; j < tempCodebook.size(); j++)
                    eu_dist.add(euclidian(inputVector, tempCodebook.get(j)));

                float smallest = 10000;
                int ci = 0;
                for (int j = 0; j < eu_dist.size(); j++) {
                    if (eu_dist.get(j) < smallest) {
                        ci = j;
                        smallest = eu_dist.get(j);
                    }
                }

                smallest = 10000;
                int cj = 0;
                for (int j = 0; j < eu_dist.size(); j++) {
                    if (j != ci) {
                        if (eu_dist.get(j) < smallest) {
                            cj = j;
                            smallest = eu_dist.get(j);
                        }
                    }
                }

                int T = (int) (float) execData.get(i).get(execData.get(i).size() - 1);
                float di = eu_dist.get(ci);
                float dj = eu_dist.get(cj);

                if (ci == cj && (float) cj == T) {
                    for (int j = 0; j < tempCodebook.get(ci).size() - 1; j++)
                        codebook.get(ci).set(j, tempCodebook.get(ci).get(j) + Parameter.EPSILON * alpha * (inputVector.get(j) - codebook.get(ci).get(j)));
                    for (int j = 0; j < tempCodebook.get(cj).size() - 1; j++)
                        codebook.get(cj).set(j, tempCodebook.get(cj).get(j) + Parameter.EPSILON * alpha * (inputVector.get(j) - codebook.get(cj).get(j)));
                } else if (ci != cj) {
                    if (ci == T || cj == T) {
                        if (minimumValue(di / dj, dj / di) > (1 - Parameter.WINDOW_SIZE) / (1 + Parameter.WINDOW_SIZE)) {
                            if (ci == T) {
                                for (int j = 0; j < tempCodebook.get(ci).size() - 1; j++)
                                    codebook.get(ci).set(j, tempCodebook.get(ci).get(j) + alpha * (inputVector.get(j) - codebook.get(ci).get(j)));
                                for (int j = 0; j < tempCodebook.get(cj).size() - 1; j++)
                                    codebook.get(cj).set(j, tempCodebook.get(cj).get(j) - alpha * (inputVector.get(j) - codebook.get(cj).get(j)));
                            } else if (cj == T) {
                                for (int j = 0; j < tempCodebook.get(ci).size() - 1; j++)
                                    codebook.get(ci).set(j, tempCodebook.get(ci).get(j) - alpha * (inputVector.get(j) - codebook.get(ci).get(j)));
                                for (int j = 0; j < tempCodebook.get(cj).size() - 1; j++)
                                    codebook.get(cj).set(j, tempCodebook.get(cj).get(j) + alpha * (inputVector.get(j) - codebook.get(cj).get(j)));
                            }
                        }
                    }
                }
            }
            alpha -= alpha * 0.005f;
        }
        //test performance
        ArrayList<LVQ3.LVQResult> lvqResultArray = new ArrayList();
        Dictionary dictionary = new Hashtable();
        //dictionary.put("Target", "Result");

        float numOfTruth = 0;
        for (int i = 0; i < testData.size(); i++) {
            //System.out.println("Data ke-" + (i + 1));
            lvqResultArray.add(new LVQ3.LVQResult(testData.get(i).get(testData.get(i).size() - 1),
                    nearestClass(codebook, testData.get(i))));
            System.out.println("The Target");
            System.out.println(testData.get(i).get(testData.get(i).size() - 1));
            System.out.println("The Result");
            System.out.println(nearestClass(codebook, testData.get(i)));
            if (testData.get(i).get(testData.get(i).size() - 1) == nearestClass(codebook, testData.get(i))) {
                numOfTruth++;
            }
        }

        //System.out.println(random.);
        System.out.println("The Num of Truth " + (numOfTruth / (float) testData.size()));
    }

    public static boolean indexAlreadAdded(int index, ArrayList<Integer> indexes){
        for(int singleInt : indexes)
            if(index == singleInt)
                return true;
        return false;
    }

    public static int numOfClass(ArrayList<ArrayList<Float>> data, float classA){
        int num = 0;
        for(ArrayList<Float> singleData : data){
            if(singleData.get(singleData.size()-1) == classA)
                num++;
        }
        return num;
    }

    public static class LVQResult{
        public Float target;
        public Float result;
        public LVQResult(Float target, Float result){
            this.target = target;
            this.result = result;
        }

        public LVQResult() {
        }

        public Float getTarget() {
            return target;
        }

        public void setTarget(Float target) {
            this.target = target;
        }

        public Float getResult() {
            return result;
        }

        public void setResult(Float result) {
            this.result = result;
        }
    }

    //method needed
    public static int getNumOfClass(ArrayList<ArrayList<Float>> dataset){
        HashSet<Float> classSet = new HashSet<>();
        for(ArrayList<Float> list : dataset){
            classSet.add(list.get(list.size()-1));
        }
        return classSet.size();
    }

    public static boolean stillCanAdd(ArrayList<ArrayList<Float>> dataset, ArrayList<Integer> randomNumber, int toSearch, int maxNum){
        ArrayList<Float> species = new ArrayList<>();
        for(int i = 0; i < randomNumber.size(); i++) {
            species.add(dataset.get(randomNumber.get(i)).get(dataset.get(0).size()-1));
        }
        int num = 0;
        for(int i = 0; i < species.size(); i++){
            if((float)species.get(i) == (float)dataset.get(toSearch).get(dataset.get(0).size()-1)){
                num++;
            }
        }
        if(num < maxNum) return true;
        return false;
    }

    public static ArrayList<ArrayList<Float>> copyArray( ArrayList<ArrayList<Float>> array){
        ArrayList<ArrayList<Float>> arrayToReturn = new ArrayList<>();
        for(int i = 0; i < array.size(); i++){
            arrayToReturn.add(new ArrayList<Float>());
            ArrayList<Float> currentArray = arrayToReturn.get(i);
            for(int j = 0; j < array.get(i).size(); j++){
                currentArray.add(array.get(i).get(j));
            }
        }
        return arrayToReturn;
    }

    public static float minimumValue(float a1, float a2){
        if(a1 < a2) return a1;
        return a2;
    }

    public static float euclidian(ArrayList<Float> X, ArrayList<Float> W){

        float sum = 0;
        for(int i = 0; i < X.size()-1; i++){
            sum += Math.pow((W.get(i)- X.get(i)), 2);
        }
        sum = (float)Math.sqrt(sum);
        return sum;
    }

    public static float nearestClass(ArrayList<ArrayList<Float>> codebook, ArrayList<Float> inputVector){
        float smallest = 10000;
        int ci = 0;
        int index = 0;

        for(ArrayList<Float> cbook : codebook){
            float euclidianResult = euclidian(inputVector, cbook);
            if(euclidianResult < smallest){
                smallest = euclidianResult;
                ci = index;
            }
            index++;
        }
        return codebook.get(ci).get(codebook.get(ci).size()-1);
    }

    public static float percentageOfTruth(ArrayList<LVQ3.LVQResult> test){
        float num = 0;
        for(LVQ3.LVQResult res : test){
            if((float)res.result == (float)res.target){
                num++;
            }
        }
        return num;
    }

    public void kFoldCross(int num_k){
        //misal ada 15 data

    }
}
