package com.richardo.lvq3endpoint.configure;

import com.richardo.lvq3endpoint.model.Users;
import com.richardo.lvq3endpoint.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class MyUserDetailServices implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Users user = userRepo.getUserByEmail(email);
        if(user == null){
            throw new UsernameNotFoundException("Not Found");
        }

        return new MyUserDetails(user);
    }
}
