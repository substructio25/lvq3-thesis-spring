package com.richardo.lvq3endpoint.configure;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(annotations = Controller.class)
public class AuthenticationAdvice {
    @ModelAttribute("isAuth")
    public String getTest(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "kusuma";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
            System.out.println(username);
        } else {
            username = principal.toString();
            System.out.println(username);
        }
        return username;
    }
}

