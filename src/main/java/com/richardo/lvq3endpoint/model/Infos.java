package com.richardo.lvq3endpoint.model;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "infos", schema = "deficiency")
public class Infos {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String benefits;
    private String sources;
    private String content_sources;
    @CreationTimestamp
    private Date added_at;
    public int micronutrient_id;
    public String img_source;
}
