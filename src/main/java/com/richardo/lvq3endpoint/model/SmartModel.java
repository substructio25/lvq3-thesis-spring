package com.richardo.lvq3endpoint.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "smartmodel", schema = "deficiency")
public class SmartModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @OneToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    public Users users;
    public float learning_rate;
    public float min_learning_rate;
    public float win_size;
    public float epsilon;
    public float performance;
    public String codebook;
    @CreationTimestamp
    public Date created_at;
}
