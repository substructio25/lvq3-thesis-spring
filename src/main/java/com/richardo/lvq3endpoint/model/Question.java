package com.richardo.lvq3endpoint.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created by Richardo Kusuma on 13/08/2019.
 */
@Entity
@Data
@Table(name="questions", schema = "deficiency")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String question;

    @OneToOne
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "symtompid")
    public CommonSymtomp commonSymtomp;
}
