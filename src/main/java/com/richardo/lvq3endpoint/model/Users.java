package com.richardo.lvq3endpoint.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="users", schema = "deficiency")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String username;
    public String password;
    public String email;
    public int weight;
    public int height;
    public int gender;
    public boolean enabled;
}
