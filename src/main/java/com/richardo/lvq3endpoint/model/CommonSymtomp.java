package com.richardo.lvq3endpoint.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Richardo Kusuma on 13/08/2019.
 */
@Entity
@Data
@Table(name = "commonsymtomps", schema = "deficiency")
public class CommonSymtomp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String symtomp;
}
