package com.richardo.lvq3endpoint.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="authorities", schema = "deficiency")
public class Authorities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    public String username;
    public String authority;
}
