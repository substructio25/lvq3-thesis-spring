package com.richardo.lvq3endpoint.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name="symtomps", schema = "deficiency")
public class Symtomps {
    @Id
    public int id;
    public String symtomp;
}
