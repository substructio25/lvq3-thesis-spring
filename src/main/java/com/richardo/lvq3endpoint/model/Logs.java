package com.richardo.lvq3endpoint.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Logs{
    public String classString;
    public String targetString;
    public Logs(String classString, String targetString){
        this.classString = classString;
        this.targetString = targetString;
    }
}