package com.richardo.lvq3endpoint.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="history", schema = "deficiency")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public int user_id;
    public String result;
    public String answers_pattern;
    @CreationTimestamp
    public Date checkdate;
}
